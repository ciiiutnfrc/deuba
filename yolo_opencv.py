#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2020 jaarac <javierredolfi@gmail.com>
#
# Distributed under terms of the GPLv3 license.


from __future__ import absolute_import
from __future__ import print_function

import argparse
import cv2
import numpy as np

print("Press 'Esc' to quit.")

parser = argparse.ArgumentParser(add_help=False)
parser.add_argument("--config", default='modelos/cfg/yolov3-tiny_csb-ccb.cfg',
                    help="YOLO config file")
parser.add_argument("--weights",
                    default='modelos/yolov3-tiny_csb-ccb_final.weights',
                    help="YOLO weights file")
parser.add_argument("--names", default='modelos/cfg/barbijo.names',
                    help="class names file")
args = parser.parse_args()

CONF_THRESH, NMS_THRESH = 0.5, 0.5

net = cv2.dnn.readNetFromDarknet(args.config, args.weights)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)

layers = net.getLayerNames()
output_layers = [layers[i[0] - 1] for i in net.getUnconnectedOutLayers()]

colors = ((0, 255, 0), (0, 0, 255))

cap = cv2.VideoCapture(0)
while(1):
    ret, img = cap.read()

    height, width = img.shape[:2]

    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), swapRB=True,
                                 crop=False)
    net.setInput(blob)
    layer_outputs = net.forward(output_layers)

    class_ids, confidences, b_boxes = [], [], []
    for output in layer_outputs:
        for detection in output:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]

            if confidence > CONF_THRESH:
                center_x, center_y, w, h = (detection[0:4] * np.array([width,
                                                                       height,
                                                                       width,
                                                                       height])).astype('int')

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                b_boxes.append([x, y, int(w), int(h)])
                confidences.append(float(confidence))
                class_ids.append(int(class_id))

    indices = cv2.dnn.NMSBoxes(b_boxes, confidences, CONF_THRESH,
                               NMS_THRESH)
    if(len(indices) != 0):
        indices = indices.flatten().tolist()

        with open(args.names, "r") as f:
            classes = [line.strip() for line in f.readlines()]

        for index in indices:
            x, y, w, h = b_boxes[index]
            cv2.rectangle(img, (x, y), (x + w, y + h),
                          colors[class_ids[index]], 2)
            cv2.putText(img, classes[class_ids[index]], (x + 5, y + 20),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 1,
                        colors[class_ids[index]], 2)

    cv2.imshow('window', img)
    key = cv2.waitKey(10)
    if(key == 27):
        break

cv2.waitKey(0)
cv2.destroyAllWindows()
