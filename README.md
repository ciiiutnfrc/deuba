# DEUBa: Detector de utilización de barbijos

<img src="resultado.png" alt="drawing" width="640"/>


Dataset con imágenes de personas utilizando protectores buco-nasales.

## Descripción del conjunto de datos:
Este conjunto de datos fue elaborado
utilizando 11 diferentes videos disponibles en la plataforma
YouTube, en donde se muestran personas utilizando máscaras
de protección en diversas situaciones y entornos: videos de
trabajadores en las industrias, personas en la vı́a pública, en-
trevistas a personas y niños en las escuelas. Este conjunto está
compuesto por 1 720 imágenes con 1 261 caras con protectores
buconasal y 993 caras sin ellos. Para su construcción se utilizó
el software Label Me en donde cada etiqueta posee solamente
2 atributos: a) utilización de protector buconasal y b) posición
de la cara en la imagen.

Apartir de [notebook](/dataset/DEUBa_dataset.ipynb) se pueden obtener la información para analizar el dataset. 


|  Video  |    Descripción   | N° de imágenes |               |            | Resolución |        | N° de etiquetas |     | Tamaño de los recuadros |           |                |
|:-------:|:----------------:|:--------------:|:-------------:|:----------:|:----------:|:------:|:---------------:|:---:|:-----------------------:|-----------|----------------|
|         |                  |      Total     | Entrenamiento | Validación |    Ancho   |  Largo |       CCB       | CSB |          Máximo         |   Mínimo  |    Promedio    |
|  Video1 | Entorno Urbano 1 |       246      |      197      |     49     |    640.0   |  360.0 |       276       |  88 |        (117, 102)       |  (11, 17) |  (51.0, 58.0)  |
|  Video2 | Entorno Urbano 2 |       196      |      157      |     39     |    640.0   |  360.0 |       241       |  0  |        (339, 537)       |  (24, 29) | (119.0, 151.0) |
|  Video3 | Entorno Urbano 3 |       196      |      157      |     39     |    640.0   |  360.0 |       298       |  97 |        (326, 304)       |  (21, 28) |  (69.0, 85.0)  |
|  video4 | Entorno Urbano 4 |       124      |      100      |     24     |   1920.0   | 1080.0 |       111       |  74 |        (165, 214)       |  (13, 12) |  (69.0, 94.0)  |
|  video5 | Entorno Urbano 5 |       92       |       74      |     18     |   1080.0   | 1080.0 |       130       |  50 |        (200, 178)       |  (13, 18) |  (58.0, 71.0)  |
|  video6 |   Entrevista 1   |       27       |       22      |      5     |   1920.0   | 1080.0 |        23       |  6  |        (385, 469)       |  (29, 35) | (251.0, 342.0) |
|  video7 | Entorno Fabril 1 |       114      |       92      |     22     |    480.0   |  360.0 |        86       |  40 |        (517, 769)       |  (26, 38) | (152.0, 196.0) |
|  Video8 | Entorno Fabril 2 |       321      |      257      |     64     |    400.0   |  224.0 |       246       | 128 |       (1296, 1070)      |  (43, 55) | (229.0, 297.0) |
|  video9 |      Escuela     |       91       |       73      |     18     |   1280.0   |  720.0 |       126       |  23 |        (158, 179)       |  (29, 37) |  (64.0, 77.0)  |
| Video10 | Entorno Fabril 3 |       139      |      112      |     27     |   1920.0   | 1080.0 |        62       |  80 |        (169, 252)       |  (48, 59) | (106.0, 153.0) |
| Video11 |   Entrevista 2   |       174      |      140      |     34     |   1280.0   |  720.0 |       210       |  13 |        (333, 449)       | (86, 121) | (152.0, 229.0) |


## Descripción del modelo utilizado:
Se utilizó la versión tiny de la red YoloV3 para realizar las detecciones. Se generó
el [modelo](modelos/yolov3-tiny_csb-ccb_final.weights) utilizando las imágenes de Entrenamiento del Dataset.

<img src="/modelos/chart_yolov3-tiny_csb-ccb.png" alt="drawing" width="480"/>

```
calculation mAP (mean average precision)...
176
 detections_count = 341, unique_truth_count = 295  
class_id = 0, name = ccb, ap = 86.71%            (TP = 195, FP = 9) 
class_id = 1, name = csb, ap = 93.20%            (TP = 52, FP = 3) 

 for conf_thresh = 0.25, precision = 0.95, recall = 0.84, F1-score = 0.89 
 for conf_thresh = 0.25, TP = 247, FP = 12, FN = 48, average IoU = 77.71 % 

 IoU threshold = 50 %, used Area-Under-Curve for each unique Recall 
 mean average precision (mAP@0.50) = 0.899544, or 89.95 % 
Total Detection Time: 1 Seconds

```

## Ejemplo de funcionamiento (GNU/Linux SO):
Para correr un ejemplo de detección usando la WebCam ejecute los siguientes
comandos en la línea de comandos:
```bash
git clone https://gitlab.com/ciiiutnfrc/deuba
cd deuba
python yolo_opencv.py
```
El código anterior supone que están instaladas las librerías OpenCV.

## Paper
https://latamt.ieeer9.org/index.php/transactions/article/view/4378

### Citación
D. Gonzalez Dondo, J. A. Redolfi, R. G. Araguás, and D. Garcia, “Application of Deep-Learning Methods to Real Time Face Mask Detection”, IEEE LAT AM T, vol. 19, no. 6, pp. 994–1001, Jun. 2021.
 
 ### Licencia
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Dataset" property="dct:title" rel="dct:type">DEUBa</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ciiiutnfrc/deuba" property="cc:attributionName" rel="cc:attributionURL">Diego Gonzalez Dondo, Javier Andres Redolfi, Gastón Roberto Araguás</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons Atribución 4.0 Internacional</a>
